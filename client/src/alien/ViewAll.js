import React, { useState , useEffect} from "react";
import Layout from "../core/Layout";
import Profile from "./Profile";
import Checkbox from "../core/Checkbox";
import {  getCommanders, getSubordanets , getAll} from "../api";


const ViewAll = () => {
  const [myFilters, setMyFilters] = useState({
        filters: { commanders: [] }
    });
  const [commanders, setCommanders] = useState([]);
  const [error, setError] = useState(false);
  const [filteredResults, setFilteredResults] = useState([]);

  const init = () => {
        getCommanders().then(data => {
            if (data.error) {
                setError(data.error);
            } else {
                  console.log('--------commanders---------11111', data,'111111--------commanders---------');
                setCommanders(data.commanders.concat(data.chiefs));
            }
        });
        getAll().then(data => {
            if (data.error) {
                setError(data.error);
            } else {
              //console.log(data['Alien-warriors'].concat(data['Alien-commanders']).concat(data['Alien-chief-commanders']))
                setFilteredResults(data['Alien-warriors'].concat(data['Alien-commanders']).concat(data['Alien-chief-commanders']));
            }
        });
    };

    const loadFilteredResults = newFilters => {
        // console.log(newFilters);
        getSubordanets(newFilters).then(data => {
            if (data.error) {
                setError(data.error);
            } else {
                console.log("EEEEE   EEEEEE  ",data)
                setFilteredResults(data.data);

            }
        });
    };

    useEffect(() => {
            init();
        }, []);


  const handleFilters = (filters, filterBy) => {
    if(filters.length == 0){
      init();
    }else{
      console.log('filters', filters, filters==[])
      const newFilters = { ...myFilters };
      newFilters.filters[filterBy] = filters;

      loadFilteredResults(myFilters.filters);
      setMyFilters(newFilters);
    }
  };

  return (
      <Layout title="List Aliens Page" description="list all alien known !!!" className="container-fluid">
      <div className="row">
          <div className="col-4">
            <div className="col-4">
              <h4>Filter by commanders</h4>
              <ul>
                  <Checkbox
                    commanders={commanders}
                    handleFilters={filters =>
                                handleFilters(filters, "commanders")
                            }
                   />
              </ul>
            </div>
          </div>

          <div className="col-8">
          <h2 className="mb-4">Aliens</h2>
                <div className="row">
                    {filteredResults.map((alien, i) => (
                        <Profile key={i} alien={alien} />
                    ))}
                </div>
          </div>
      </div>
      </Layout>
  );
}
export default ViewAll;
