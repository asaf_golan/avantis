import React from "react";

const Profile = (alien) =>{

  const Rank =(alien)=>{
    if(alien.commander){
      return "Alien-Warrior"
    }else if(alien['membership-card']){
      return "Alien-chief-Commander"
    }else{
      return 'Alien-Commander'
    }
  }
  console.log("ALIEN " ,alien.alien)
  alien = alien.alien;
  return (
      <div className="col-4 mb-3">
          <div className="card">
              <div className="card-header">{alien.name}</div>
              <div className="card-body">
                  <p >Rank: {Rank(alien)}</p>
                  <p style={{ display: Rank(alien)=="Alien-Warrior" ? "" : "none" }}>Weapon: {alien.weapon}</p>
                  <p style={{ display: alien['membership-card'] == undefined? "" : "none" }} >Commander ID: {alien.commander? alien.commander: alien['chief-commander']}</p>
                  <p style={{ display: alien.commander == undefined? "" : "none" }} >Vehicle: {alien.vehicle}</p>
                  <p style={{ display: alien['membership-card'] != undefined? "" : "none" }}>Membership card: {alien['membership-card']}</p>
              </div>
          </div>
      </div>
  );
}


export default Profile;
