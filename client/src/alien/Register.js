import React, { useState , useEffect} from "react";
import Layout from "../core/Layout";
import Select from "../core/Select";
import { API } from "../config";
import  Profile from "./Profile";
import { register, getCommanders } from "../api";


//populate select options commanders with getcommanders method

const Register = () => {

  const [values, setValues] = useState({
      name: "",
      role: "",
      weapon: "",
      vehicle:"",
      commander:"",
      membershipCard:"",
      error: "",
      success: false,
      successObj:{},
  });

  const [commanders, setCommanders] = useState([]);
  const [chiefs, setchiefs] = useState([]);
  const [initError, setError] = useState(false);

  const init = () => {
        console.log("init() runs")
        getCommanders().then(data => {
            if (data.initError) {
                setError(data.initError);
            } else {
                console.log("commanders", data.commanders )
                setCommanders(data.commanders);
                setchiefs(data.chiefs);
            }
        });
    };

    useEffect(() => {
            init();
        }, []);



  const { successObj ,name, role, weapon,vehicle, membershipCard,commander, success, error } = values;


  const handleChange = name => event => {
      setValues({ ...values, success:false, error: false, [name]: event.target.value });
  };


  const clickSubmit = event => {
        event.preventDefault();
        register({ name, role, weapon,vehicle, membershipCard, commander }).then(data => {

          console.log("ddata", data)
            if (data.error) {

                setValues({ ...values, error: data.error , success: false });
            } else {
                setValues({
                    ...values,
                    name: "",
                    role: "",
                    weapon: "",
                    vehicle:"",
                    commander:"",
                    membershipCard:"",
                    error: "",
                    success: true,
                    successObj:data
                });
                init();
            }
        })
    };

  const registerForm = () => (
      <form>
          <div className="form-group">
              <label className="text-muted">Name</label>
              <input
                  onChange={handleChange("name")}
                  type="text"
                  className="form-control"
                  value={name}
              />
          </div>

          <div className="form-group">
              <label className="text-muted">Alien-Rank</label>
              <select className="form-control" value={role}  onChange={handleChange("role")}>
                <option value="">select</option>
                <option value="0">Alien warrior</option>
                <option value="1">Alien commander</option>
                <option value="2">Alien chief commander</option>
              </select>
          </div>
          <div className="form-group" style={{ display: role==0 && role != "" ? "" : "none" }}>
              <label className="text-muted">Weapon</label>
              <select className="form-control" value={weapon} onChange={handleChange("weapon")}>
                <option value="">select</option>
                <option value="Water gun">Water gun</option>
                <option value="Pepper spray">Pepper spray</option>
                <option value="Chopsticks">Chopsticks</option>
              </select>
          </div>
          <div className="form-group" style={{ display: role!=0 && role != "" ? "" : "none" }}>
              <label className="text-muted">Vehicle</label>
              <select className="form-control" value={vehicle}  onChange={handleChange("vehicle")}>
                <option value="">select</option>
                <option value="Bird scooter">Bird scooter</option>
                <option value="Merkava tank">Merkava tank</option>
              </select>
          </div>
          <div className="form-group" style={{ display: role==2 && role != "" ? "" : "none" }}>
              <label className="text-muted">Membership Card</label>
              <select className="form-control" value={membershipCard} onChange={handleChange("membershipCard")}>
                <option value="">select</option>
                <option value="Hitech zone">Hitech zone</option>
                <option value="Hever">Hever</option>
                <option value="Shufersal">Shufersal</option>
              </select>
          </div>
          <div className="form-group" style={{ display: role!="" && role!=2 ? "" : "none" }}>
              <label className="text-muted">Commander</label>
              <select className="form-control" value={commander} onChange={handleChange("commander")}>

                <option value="">select</option>

                <Select commanders={role==1?chiefs:commanders}/>
              </select>
          </div>
          <button  onClick={clickSubmit} className="btn btn-primary">Submit</button>
      </form>
  );

  const showError = () => (
      <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
      >
          {error}
      </div>
  );

const showSuccess = () => {
  if(success==true){
    return(
          <div
              className="alert alert-info"
              style={{ display: success ? "" : "none" }}
          >
            New ALIEN is created. :-)
              <Profile alien = {successObj}/>


          </div>
      );
  }
}
    return(
      <Layout title="Register Page" description="register a new alien" className="container col-md-8 offset-md-2">
          {showSuccess()}
          {showError()}
          { registerForm() }
      </Layout>
    )
}


export default Register;
