import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Register from "./alien/Register";
import ViewAll from "./alien/ViewAll";


const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>

                <Route path="/" exact component={ViewAll} />
                <Route path="/register" exact component={Register} />
            </Switch>
        </BrowserRouter>
    );
};

export default Routes;
