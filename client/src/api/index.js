import { API } from "../config";


   const register = alien => {
        return fetch(`${API}/register`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(alien)
        })
            .then(response => {
                return response.json();
            })
            .catch(err => {
                console.log(err);
            });
    };

    const getCommanders = ()=> {

            return fetch(`${API}/commanders`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
            })
                .then(response => {

                    return response.json();
                })
                .catch(err => {
                    console.log(err);
                });
        };

        const getAll = ()=> {

                return fetch(`${API}/`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json"
                    },
                })
                    .then(response => {

                        return response.json();
                    })
                    .catch(err => {
                        console.log(err);
                    });
            };

        const getSubordanets = (commandersIds)=> {
          console.log('commandersIds', commandersIds)
          return fetch(`${API}/commander`, {
              method: "POST",
              headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
              },
              body: JSON.stringify(commandersIds)
          })
              .then(response => {
                  return response.json();
              })
              .catch(err => {
                  console.log(err);
              });
            };

        export {
          getSubordanets,
          getCommanders,
          register,
          getAll,
        }
