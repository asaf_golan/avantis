import React, { useState, useEffect } from "react";

const Select = ({ commanders }) => {
    return commanders.map((c, i) => (
        <option key={i} value={c.id}>{c.name}</option>
    ));
};

export default Select;
