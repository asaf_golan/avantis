import React, { useState, useEffect } from "react";

const Checkbox = ({ commanders, handleFilters }) => {

  const [checked, setCheked] = useState([]);

  const handleToggle = c => () => {
      // return the first index or -1
      const currentCommanderId = checked.indexOf(c);
      const newCheckedCommanderId = [...checked];
      // if currently checked was not already in checked state > push
      // else pull/take off
      if (currentCommanderId === -1) {
          newCheckedCommanderId.push(c);
      } else {
          newCheckedCommanderId.splice(currentCommanderId, 1);
      }
      //console.log(newCheckedCommanderId);
      setCheked(newCheckedCommanderId);
      handleFilters(newCheckedCommanderId);

  };
    console.log('--------commanders---------', commanders,'--------commanders---------');
    return commanders.map((c, i) => (
        <li key={i} className="list-unstyled">
            <input
                onChange={handleToggle(c.id)}
                value={checked.indexOf(c.id === -1)}
                type="checkbox"
                className="form-check-input"
            />
            <label className="form-check-label">{c.name}</label>
        </li>
    ));
};

export default Checkbox;
