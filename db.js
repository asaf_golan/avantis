module.exports = {
  "alien-warriors": [
    {
      "id": 1,
      "name": "Aurra Belanna",
      "weapon": "Water gun",
      "commander": 4
    },
    {
      "id": 2,
      "name": "Cando Dash",
      "weapon": "Water gun",
      "commander": 4
    },
    {
      "id": 3,
      "name": "Talon Makkan",
      "weapon": "Pepper spray",
      "commander": 5
    }
  ],
  "alien-commanders": [
    {
      "id": 4,
      "name": "Malcom Soval",
      "vehicle": "Bird scooter",
      "chief-commander": 6
    },
    {
      "id": 5,
      "name": "Fizzus Startakus",
      "vehicle": "Bird scooter",
      "chief-commander": 6
    }
  ],
  "alien-chief-commanders": [
    {
      "id": 6,
      "name": "Rinya Galen",
      "vehicle": "Merkava tank",
      "membership-card": "Shufersal"
    }
  ]
}
