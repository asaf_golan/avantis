exports.AlienModel = (alienObj) => {


  alien = {}
  error={}
  alien.id = alienObj.id
  alien.name =alienObj.name

  if(alienObj.role== 0){
    alien.commander= alienObj.commander
    alien.weapon = alienObj.weapon
  }else if(alienObj.role== 1){
    alien['chief-commander']= alienObj.commander
    alien.vehicle = alienObj.vehicle
  }else if(alienObj.role== 2){
    alien['membership-card']= alienObj['membershipCard']
    alien.vehicle = alienObj.vehicle
  }

  return alien
};
