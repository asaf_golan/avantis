
const {check, checkIf, validationResult} = require('express-validator/check');

const weapons = ['Water gun','Pepper spray','Chopsticks']
const vehicles = ['Bird scooter', 'Merkava tank']
const membershipCards = ['Hitech zone','Hever','Shufersal']
const roles =[0,1,2]


exports.registerValidator = [
  check("name", "Name is required").notEmpty(),
  check('role', 'rank is required').isIn(roles)
  .custom((value, { req }) => {
    if(value == 0){
      if (weapons.indexOf(req.body.weapon)==-1) {
        throw new Error('there is no such weapon');
      }
      return true
    }
    else if(value == 1 || value == 2){
      if (vehicles.indexOf(req.body.vehicle)==-1) {
        throw new Error('there is no such vehicle');
      }
      return true
    }
    else if(value == 2){
      if (membershipCards.indexOf(req.body['membership-cards'])==-1) {
        throw new Error('there is no such membership');
      }
      return true
    }
  }),




  //check('password').exists(),
  (req, res, next) => {
    try {
      validationResult(req).throw();
      next();
    } catch (err) {
      console.log('err.errors[0]', err.errors, '----------------------')
      res.status(200).json({ error : err.errors[0].msg });
    }
  }
];
