const db = require("../../db.js");
const { AlienModel} = require("../models/Alien.js");

// show all the "alien-warriors" in the record set
//db["alien-warriors"].forEach(alien => console.log(alien.name))

function generateId() {
  return db["alien-warriors"].length + db["alien-chief-commanders"].length + db["alien-commanders"].length + 1;
}

function createSubordinatesArr(commanderId){
  const subordinates =[]

    db["alien-warriors"].forEach((alien) => {
      if (alien.commander == commanderId) {
        subordinates.push(alien)
      }
    })

    db["alien-commanders"].forEach((alien) => {
      if (alien['chief-commander'] == commanderId) {
        subordinates.push(alien)
      }
    })

  return subordinates;
}

//iterates only for chiefs/commanders array
exports.alienById = (req, res, next, id) => {

  db["alien-chief-commanders"].forEach(alien => {

    if (alien.id == id) {
      req.alien = alien;
      req.alien.role = 2;
      next();
    }
  });
  db["alien-commanders"].forEach(alien => {
    if (alien.id == id) {
      req.alien = alien;
      req.alien.role = 1;
      next();
    }
  });

  if(!req.alien && req.body.role !=2){
    res.json({
      error: "no such commander found"
    })
  }else{
    next();
  }
};


exports.listAll = (req, res) => {
  res.json({
    'Alien-warriors': db["alien-warriors"],
    'Alien-commanders': db["alien-commanders"],
    'Alien-chief-commanders': db["alien-chief-commanders"]
  });
};

exports.showcommanders = (req, res) => {
  console.log("######", {chiefs: db["alien-chief-commanders"], commanders:( db["alien-commanders"])},"########")
  res.json({
    chiefs: db["alien-chief-commanders"], commanders:( db["alien-commanders"])
  });
};


exports.register = (req, res) => {
  req.error = undefined;
  //Alien instanciate
  let newAlien = req.body
  var commanderId =  newAlien.commander? newAlien.commander: newAlien['chief-commander'];
  var err;
  // MOVE TO VALIDATION FILE validate commander & min pple under same commander in hirerchy
  exports.alienById(req, res,()=>{
    const ppleCountMax = createSubordinatesArr(req.body.commander).length;
    console.log('req.alien', newAlien.role)
    if( newAlien.role!=2){
      if(req.alien.role - newAlien.role !=1 ){
          err = 'cant enroll as his subordante choose a different commander ( RANK ISSUE) ';
      }
      if(newAlien.role == 0 && ppleCountMax > 9 || newAlien.role == 1 && ppleCountMax > 2  ){
            err = 'cant enroll to his platoon soldier to many troops ';
      }
    }
  },commanderId)


  if(err){

    res.json({error:err});
  }else{
    newAlien.id = generateId();
    // alien from model
    newAlien = AlienModel(newAlien);
    if(newAlien.commander){
      db['alien-warriors'].push(newAlien)
    }else if(newAlien['chief-commander']){
      db['alien-commanders'].push(newAlien)
    }else{
      db['alien-chief-commanders'].push(newAlien)
    }
    
    res.json(alien)
  }

}

exports.showSubordinates = (req, res) => {
  console.log('req.body', req.body)
  var sub=[];
  req.body.commanders.forEach(commanderId=>{
    array = createSubordinatesArr(commanderId);
    sub = sub.concat(array)

  })
  res.json({
    data: sub
  })
};
