const express = require("express");
const router = express.Router();
const {registerValidator} = require("../validator/index.js");
const { listAll, register , alienById , showSubordinates, showcommanders} = require("../controllers/db-service");

router.get("/", listAll);

router.post("/register", registerValidator ,register )
router.post("/commander/",showSubordinates)
router.get("/commanders/",showcommanders)


router.param('alienId',alienById)

module.exports = router;
