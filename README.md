**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## # avantis alien invasion

React, node.js express developer task for avantis

---

## Installation


1.open terminal    
2.clone repo   
3.cd <repo>     
4.cd server    
5.npm install    
6.npm start    
   
1.open another terminal window    
2.cd <repo>     
3.cd client    
4.npm install   
5.npm start       




## Usage

Open localhost:3000/list in the browser and play around

The DB resets to db.js values if the server is restarted.

